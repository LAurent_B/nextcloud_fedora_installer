#!/bin/bash

# Installation Script by Scott Alan Miller
# Based on Installation Instructions by Jared Busch
# https://mangolassi.it/topic/16380/
# https://raw.githubusercontent.com/sorvani/scripts/master/Nextcloud/selinux_config.sh
#
# Example Use:
# source <(curl -s https://gitlab.com/scottalanmiller/nextcloud_fedora_installer/raw/master/nextcloud_fedora.sh)
#
# Current Version: NextCloud 13
# For: Fedora 27 Linux
#
# Script discussion link: https://mangolassi.it/topic/16389/nextcloud-automated-installation

# LAurent_B 07/2018
# Add comment between each part of the script
# Add database user name option
# Add Data location option
# Change name of several variables
# Add Verify Fedora version
# Add Nextclout version option
# Add nexcloud.zip file check

separator='"*******************************************************************"'


# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# Make sure only good fedora version can run our script
Fedora_version=`uname -r | cut  -f4 -d'.'`

echo 'fedora version : ' $Fedora_version

if [[ $Fedora_version != "fc28" ]]; then
   echo "Not good fedora version" 1>&2
   exit 1
fi

echo "Installing NextCloud 13 for Fedora 28 Server or Minimal"
echo "If you have not made a DNS entry for the system,"
echo "it is recommended that you do so now."
echo ""

echo $separator
echo "Install Information"
echo $separator

echo "* NextCloud Information"
echo "Enter the name of your web admin user account (ex. admin)"
read ncadminuser
echo "Enter the initial password for admin"
read ncadminpass
echo "Enter the database user name for NextCloud: "
read ncuser
echo "Enter the database password for NextCloud: "
read ncpass
echo "Enter the /data path for NextCloud: "
read ncdatapath
echo "Enter the FQDN you setup for Nextcloud (ex. nc.domain.com)"
read ncfqdn

echo "* MariaDB Information"
echo "Enter the root MariaDB user password: "
read mdbrootpass



export ncpath='/var/www/html/nextcloud'
#export datapath='/data'
export httpdrw='httpd_sys_rw_content_t'

export nc_version=nextcloud-13.0.5.zip
export nc_version_sha512=$nc_version'.sha512'


echo $separator
echo "Install dependency"
echo $separator

dnf -y install wget unzip cockpit firewalld net-tools php mariadb mariadb-server mod_ssl php-pecl-apcu httpd php-xml php-gd php-pecl-zip php-mbstring redis php-pecl-redis php-process php-pdo certbot python2-certbot-apache php-mysqlnd policycoreutils policycoreutils-python policycoreutils-python-utils dnf-automatic sysstat php-ldap php-opcache libreoffice fail2ban
dnf -y update


echo $separator
echo "Install nextcloud"
echo $separator

cd /var/www/html
wget https://download.nextcloud.com/server/releases/$nc_version
wget https://download.nextcloud.com/server/releases/$nc_version_sha512
nextcloud_integrity=`sha512sum -c $nc_version_sha512 | cut  -f2 -d' '`


# Make sure only good zip file can run our script
if [[ $nextcloud_integrity != "OK" ]]; then
   echo "zip file are bad" 1>&2
   exit 1
fi
unzip $nc_version
mkdir /var/www/html/nextcloud/data
chown apache:apache -R /var/www/html/nextcloud
mkdir /data
chown apache:apache -R /data

echo $separator
echo "Firewall configuration"
echo $separator

firewall-cmd --zone=FedoraServer --add-port=https/tcp --permanent #Server
firewall-cmd --zone=public --add-port=https/tcp --permanent       #Minimal
firewall-cmd --reload

echo $separator
echo "Mariadb configuration"
echo $separator

systemctl start mariadb
systemctl enable mariadb
systemctl start redis
systemctl enable redis

mysql -e "CREATE DATABASE nextcloud;"
mysql -e "CREATE USER '$ncuser'@'localhost' IDENTIFIED BY '$ncpass';"
mysql -e "GRANT ALL ON nextcloud.* TO '$ncuser'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"
mysql -e "UPDATE mysql.user SET Password=PASSWORD('$mdbrootpass') WHERE User='root';"
mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
mysql -e "DELETE FROM mysql.user WHERE User='';"
mysql -e "DROP DATABASE test;"
mysql -e "FLUSH PRIVILEGES;"

echo $separator
echo "SElinux configuration"
echo $separator

setsebool -P httpd_can_sendmail 1
setsebool -P httpd_can_network_connect 1
semanage fcontext -a -t ${httpdrw} "${ncpath}/config(/.*)?"
restorecon -R ${ncpath}/config
semanage fcontext -a -t ${httpdrw} "${ncpath}/apps(/.*)?"
restorecon -R ${ncpath}/apps
semanage fcontext -a -t ${httpdrw} "${ncpath}/data(/.*)?"
restorecon -R ${ncpath}/data
semanage fcontext -a -t ${httpdrw} "${ncpath}/updater(/.*)?"
restorecon -R ${ncpath}/updater
semanage fcontext -a -t ${httpdrw} "${datapath}(/.*)?"
restorecon -R ${datapath}
systemctl restart httpd
systemctl enable httpd

echo $separator
echo "Nextcloud configuration"
echo $separator

echo "/// 10-opcache.ini"

sed -i -e 's/;opcache.enable_cli=0/opcache.enable_cli=1/'                             /etc/php.d/10-opcache.ini;
sed -i -e 's/opcache.max_accelerated_files=4000/opcache.max_accelerated_files=10000/' /etc/php.d/10-opcache.ini;
sed -i -e 's/;opcache.save_comments=1/opcache.save_comments=1/'                       /etc/php.d/10-opcache.ini;
sed -i -e 's/;opcache.revalidate_freq=2/opcache.revalidate_freq=1/'                   /etc/php.d/10-opcache.ini;

echo "/// restart php-fpm"

systemctl restart php-fpm

cd $ncpath
sudo -u apache php occ maintenance:install --database "mysql" --database-name "nextcloud" --database-user $ncuser --database-pass $ncpass --admin-user $ncadminuser --admin-pass $ncadminpass --data-dir $datapath
sudo -u apache php occ config:system:set trusted_domains 1 --value=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
if [ -n "$ncfqdn" ]
then
  sudo -u apache php occ config:system:set trusted_domains 2 --value=$ncfqdn
fi

echo "/// nextcloud/config/config.php"

sed -i "$ d" /var/www/html/nextcloud/config/config.php
echo "  'memcache.locking' => '\OC\Memcache\Redis'," >> /var/www/html/nextcloud/config/config.php
echo "  'memcache.local' => '\OC\Memcache\Redis',"   >> /var/www/html/nextcloud/config/config.php
echo "      'redis' => array("                       >> /var/www/html/nextcloud/config/config.php
echo "      'host' => 'localhost',"                  >> /var/www/html/nextcloud/config/config.php
echo "      'port' => 6379,"                         >> /var/www/html/nextcloud/config/config.php
echo "      ),"                                      >> /var/www/html/nextcloud/config/config.php
echo ");"                                            >> /var/www/html/nextcloud/config/config.php

echo "/// httpd + fail2ban"

systemctl restart httpd
systemctl start fail2ban
systemctl enable fail2ban


echo "Your installation is now complete.  You can begin using your system."
echo "It is recommended that you now setup backups, and consider installing"
echo "a certificate for your site; we recommend LetsEncrypt, it is free."

#####################################
#
# ToDo
#
# 1. Automate the root and db passwords randomization
# 2. Fix SSL Warning
# 3. Data location option
# 4. Verify Fedora version
# 5. Add automated system updates
# 6. Move to BZip
# 7. Delete installer file
# 8. Optionally expose port 9090
# 9. Move output to logs
# 10. Improve verbosity of final message
